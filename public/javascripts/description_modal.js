$(document).ready(function () {
  $('.description-submit').on('click', function () {
    const id = $(this).data("id");
    const cid = $(this).data("cid")
    $.ajax({
      url: "/cards/" + cid + "/lists/" + id + "/description",
      type: "PUT",
      contentType: "application/json",
      data: JSON.stringify({ description: $('#description' + id).val() }),
    }).done(function (results) {
      document.getElementById('change' + id).innerHTML = results;
      $("#desdiv"+id).hide();
    }).fail(function (err) {
      console.log(err);
    })
  });
})