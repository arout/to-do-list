function checkPass() {
  const pass1 = document.getElementById('password');
  const pass2 = document.getElementById('cpassword');
  const message = document.getElementById('confirmMessage');
  const goodColor = "#66cc66";
  const badColor = "#ff6666";

  if (pass1.value == pass2.value) {
    message.style.color = goodColor;
    message.innerHTML = "Passwords Match!";
  } else {
    message.style.color = badColor;
    message.innerHTML = "Passwords Do Not Match!";
  }
}  