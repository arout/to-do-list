$(document).ready(function () {
  $('.listAdd').on('click', function (e) {
    e.preventDefault();
    e.stopPropagation();
    if ($(this).data('requestRunning')) {
      return;
    }
    $(this).data('requestRunning', true);
    const id = $(this).data("id");
    $.ajax({
      url: "cards/" + id + "/lists",
      type: "POST",
      contentType: "application/json",
      data: JSON.stringify({ name: $('.listName' + id).val() }),
    }).done(function (results) {
      $('.card' + id).append(results);
      $('.listName' + id).val("");
      $(this).data('requestRunning', false);
      $('.listAddition'+id).hide();
      $('.listAdditionClick').fadeIn()
    }).fail(function (err) {
      console.log(err);
    })
  })

  $('.listAdditionClick').on('click', function () {
    const id = $(this).data("id");
    $(this).hide()
    $('.listAddition'+id).fadeIn();
    $('.listAddition'+id).css('display', 'block');
  })

  $('.listRemove').on('click', function (e) {
    const id = $(this).data("id");
    e.preventDefault();
    e.stopPropagation();
    $('.listAddition'+id).hide();
    $('.listAdditionClick').fadeIn()
  })

  $('.listAdditionClick').hover( function () {
    $(this).css('opacity', 0.9)
  }, function(){
    $(this).css('opacity', 0.7)
  })

})