$(document).ready(function () {
  $('#cardAdd').on('click', function (e) {
    e.preventDefault();
    e.stopPropagation();
    if ($(this).data('requestRunning')) {
      return;
    }
    $(this).data('requestRunning', true);
    $.ajax({
      url: "/cards",
      type: "POST",
      contentType: "application/json",
      data: JSON.stringify({ name: $('#cardName').val() }),
    }).done(function (results) {
      $('#cardGroup').append(results);
      $('#cardName').val("");
      $(this).data('requestRunning', false);
      $('#cardAddition').hide();
      $('#cardAdditionClick').fadeIn()
    }).fail(function (err) {
      console.log(err);
    })
  })

  $('#cardAdditionClick').on('click', function () {
    $(this).hide()
    $('#cardAddition').fadeIn();
    $('#cardAddition').css('display', 'block');
  })

  $('#cardRemove').on('click', function (e) {
    e.preventDefault();
    e.stopPropagation();
    $('#cardAddition').hide();
    $('#cardAdditionClick').fadeIn()
  })

  $('#cardAdditionClick').hover( function () {
    $(this).css('opacity', 0.9)
  }, function(){
    $(this).css('opacity', 0.7)
  })
})