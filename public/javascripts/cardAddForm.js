$(document).ready(function () {

  $(".cardname").hide();

  $(".add").hover(function () {
    const id = $(this).data("id");
    $(this).css("background-color", "#b4b4b9");
    $(this).css("cursor", "pointer");
    $(this).css("border-radius", "3px");
  }, function () {
    $(this).css("background-color", "gainsboro");
  })

  $(".add").click(function () {
    const id = $(this).data("id");
    $(".add" + id).hide();
    $(".cardname" + id).fadeIn();
  });

  $(".cancel").click(function () {
    const id = $(this).data("id");
    $(".cardname" + id).hide();
    $(".add" + id).show();
  });

})

