function ValidateEmail() {
  const email = document.getElementById('emailc');
  const message = document.getElementById('confirmMessage2');

  if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email.value)) {
    message.style.color = "#66cc66";
    message.innerHTML = "";
  } else {
    message.style.color = "#ff6666";
    message.innerHTML = "Invalid mail address";
  }
}