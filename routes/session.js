const express = require('express');
const router = express.Router();
const sessionsController = require('../controllers/sessionsController.js');

//signup routes
router.get('/sessions/signup', sessionsController.signup);
router.post('/sessions/signup', sessionsController.createUser);

//login routes
router.get('/sessions/new', sessionsController.login);
router.post('/sessions', sessionsController.create);

//logout route
router.delete('/sessions', sessionsController.delete);

module.exports = router;