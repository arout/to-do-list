const express = require('express');
const router = express.Router();
const usersController = require('../controllers/usersController.js');

router.get('/users/:id', usersController.show);
router.put('/users/:id', usersController.update);
router.put('/users/:id/password', usersController.passwordChange);

module.exports = router;