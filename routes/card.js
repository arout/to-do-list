const express = require('express');
const router = express.Router();
const cardsController = require('../controllers/cardsController.js');

router.post('/cards', cardsController.create);
router.put('/cards/:id/archive', cardsController.archive);
router.put('/cards/:id', cardsController.update);
router.put('/cards/:id/restore', cardsController.restore);
router.delete('/cards/:id', cardsController.delete);

module.exports = router;