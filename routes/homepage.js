const express = require('express');
const router = express.Router();
const homepageController = require('../controllers/homepageController.js');

//homepage route
router.get('/', homepageController.show);

module.exports = router;