const express = require('express');
const router = express.Router();
const dashboardController = require('../controllers/dashboardController.js');

//homepage route
router.get('/dashboard', dashboardController.show);

module.exports = router;