const express = require('express');
const router = express.Router();
const listsController = require('../controllers/listsController.js');

router.post('/cards/:card_id/lists', listsController.create);

router.put('/cards/:card_id/lists/:id/archive', listsController.archive);

router.put('/cards/:card_id/lists/:id/description', listsController.descriptionUpdate);

router.put('/cards/:card_id/lists/:id/restore', listsController.restore);

router.delete('/cards/:card_id/lists/:id', listsController.delete);

module.exports = router;