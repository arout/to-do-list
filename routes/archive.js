const express = require('express');
const router = express.Router();
const archivesController = require('../controllers/archivesController.js');

router.get('/archives', archivesController.show);

module.exports = router;