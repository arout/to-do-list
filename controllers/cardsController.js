const models = require('../models');
const updateCard = require('../helper/update')

class CARDSCONTROLLER {
  //create new card and store it in the database
  static async create(req, res) {
    if (!(req.session && req.session.user)) {
      return res.redirect('/');
    }
    const addCard = await models.card.build({
      name: req.body.name,
      user_id: req.session.user.id
    });
    const card = await addCard.save()
    res.render('cards/cardAdd.ejs', { name: card.name, id: card.id });
  }

  //update name of the card
  static async update(req, res) {
    if (!(req.session && req.session.user)) {
      return res.redirect('/');
    }
    const card = await models.card.findCardById(req.params.id);
    await updateCard.attributesUpdate(card, { name: req.body.modifydo });
    res.redirect('/dashboard');
  }

  //archive a card by setting archive attribute to true
  static async archive(req, res) {
    if (!(req.session && req.session.user)) {
      return res.redirect('/');
    }
    const card = await models.card.findCardById(req.params.id);
    await updateCard.attributesUpdate(card, { archive: true });
    res.redirect('/dashboard');
  }

  //restore a card from archives by setting archive attribute to false
  static async restore(req, res) {
    if (!(req.session && req.session.user)) {
      return res.redirect('/');
    }
    const card = await models.card.findCardById(req.params.id);
    await updateCard.attributesUpdate(card, { archive: false });
    res.redirect('/archives');
  }

  //delete a card from the database
  static async delete(req, res) {
    if (!(req.session && req.session.user)) {
      return res.redirect('/');
    }
    await models.list.destroy({ where: { card_id: req.params.id } })
    await models.card.destroy({ where: { id: req.params.id } })
    res.redirect('/archives');
  }
}

module.exports = CARDSCONTROLLER;