const models = require('../models');
const updateList = require('../helper/update');

class LISTCONTROLLER {
  //create a new list
  static async create(req, res) {
    if (!(req.session && req.session.user)) {
      return res.redirect('/')
    }
    const addList = models.list.build({
      name: req.body.name,
      description: "no description",
      card_id: req.params.card_id
    });
    const list = await addList.save()
    res.render('lists/listAdd.ejs', { name: list.name, id: list.id, description: list.description, cid: list.card_id });
  }

  //archive a list by setting archive attribute to true
  static async archive(req, res) {
    if (!(req.session && req.session.user)) {
      return res.redirect('/')
    }
    const list = await models.list.findListById(req.params.id);
    await updateList.attributesUpdate(list, { archive: true });
    res.redirect('/dashboard');
  }

  //update the description of a list
  static async descriptionUpdate(req, res) {
    if (!(req.session && req.session.user)) {
      return res.redirect('/')
    }
    const list = await models.list.findListById(req.params.id);
    const newList = await updateList.attributesUpdate(list, {
      description: req.body.description
    });
    res.render('lists/description.ejs', { description: newList.description });
  }

  //restore a list by setting the archive attribute to false
  static async restore(req, res) {
    if (!(req.session && req.session.user)) {
      return res.redirect('/')
    }
    const list = await models.list.findListById(req.params.id);
    await updateList.attributesUpdate(list, { archive: false });
    res.redirect('/archives');

  }

  //delete a list from the database
  static async delete(req, res) {
    if (!(req.session && req.session.user)) {
      return res.redirect('/')
    }
    await models.list.destroy({ where: { id: req.params.id } })
    res.redirect('/archives');
  }
}

module.exports = LISTCONTROLLER;