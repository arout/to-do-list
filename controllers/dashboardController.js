const models = require('../models');

class DASHBOARDCONTROLLER {
  static async show(req, res){
    if (!(req.session && req.session.user)) {
      return res.redirect('/');
    }
    const cards = await models.card.findAll({
      where: {
        user_id: req.session.user.id,
        archive: false
      }
    });
    for (let card of cards) {
      const lists = await models.list.findAll({
        where: {
          card_id: card.id,
          archive: false
        }
      });
      card.lists = lists;
    }
    res.render("dashboard/index.ejs", { user: req.session.user, data: cards });
  }
}

module.exports = DASHBOARDCONTROLLER