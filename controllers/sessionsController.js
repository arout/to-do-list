const models = require('../models');
const newUser = require('../helper/authentication');


class SESSIONSCONTROLLER {
  static signup(req, res) {
    if (!(req.session && req.session.user)) {
      res.render("session/signup.ejs", { error: "" });
    }
    res.redirect('/dashboard');
  }

  static async createUser(req, res) {
    if (req.session && req.session.user) {
      res.redirect('/dashboard');
    }
    const userName = await newUser.authenticate(req, res)
    if (!userName) {
      return res.render("session/signup.ejs", { error: 'User name must be unique' });
    }
    const userPassword = await newUser.passwordAuthenticate(req, res)
    if (!userPassword) {
      return res.render("session/signup.ejs", { error: 'password and confirm password must be same' });
    }
    const userEmail = await newUser.emailAuthenticate(req, res)
    if (!userEmail) {
      return res.render("session/signup.ejs", { error: 'email is not valid or not unique' });
    }
    const addUser = models.user.build({
      userName: req.body.user_name,
      password: req.body.password,
      firstName: req.body.first_name,
      lastName: req.body.last_name,
      email: req.body.email
    });
    const user = await addUser.save();
    req.session.user = user;
    res.redirect('/dashboard');
  }

  static login(req, res) {
    if (!(req.session && req.session.user)) {
      res.render("session/login.ejs", { error: '' });
    }
    res.redirect('/dashboard')
  }

  static async create(req, res) {
    const userLogin = await models.user.authenticate(req.body.user_name);
    if (!userLogin || !(userLogin.validPassword(req.body.password))) {
      return res.render('session/login.ejs', { error: 'Invalid user name or password.' });
    }
    req.session.user = userLogin;
    res.redirect('/dashboard');
  }

  static delete(req, res) {
    req.session.reset();
    res.redirect('/');
  }
}

module.exports = SESSIONSCONTROLLER;