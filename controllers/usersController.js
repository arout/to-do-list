const models = require('../models');
const bcrypt = require('bcrypt');
const updateUser = require('../helper/update');
const editUser = require('../helper/authentication')

class USERSCONTROLLER {
  static show(req, res) {
    if (!(req.session && req.session.user)) {
      return res.redirect('/');
    }
    res.render('user/show.ejs', { user: req.session.user, error: "" })
  }

  static async update(req, res) {

    if (!(req.session && req.session.user)) {
      return res.redirect('/');
    }
    const userName = await editUser.authenticateById(req, res)
    if (!userName) {
      return res.render('user/show.ejs', { user: req.session.user, error: "User name is not unique" });
    }
    const userEmail = await editUser.emailAuthenticateById(req, res)
    if (!userEmail) {
      return res.render('user/show.ejs', { user: req.session.user, error: "email is not valid or not unique" });
    }
    const user = await models.user.findUserById(req.session.user.id);
    const newUser = await updateUser.attributesUpdate(user, {
      userName: req.body.userName,
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      email: req.body.email
    })
    req.session.user = newUser;
    res.redirect('/users/' + req.session.user.id)
  }

  static async passwordChange(req, res) {
    if (!(req.session && req.session.user)) {
      return res.redirect('/');
    }
    const userPassword = await editUser.passwordAuthenticateById(req, res)
    if (!userPassword) {
      return res.render('user/show.ejs', { user: req.session.user, error: "passwords don't match" });
    }
    const hash = await bcrypt.hash(req.body.password, 10)
    const user = await models.user.findUserById(req.session.user.id);
    const newUser = await updateUser.attributesUpdate(user, { password: hash })
    req.session.user = newUser;
    res.render('user/show.ejs', { user: req.session.user, error: "passwords changed" });
  }
}

module.exports = USERSCONTROLLER;