var models = require('../models');
var IsEmail = require("isemail");

//User signup
exports.signup = function (req, res) {
  if (req.session && req.session.user) {
    res.redirect('/dashboard');
  } else res.render("session/signup.ejs", { error: "" });
}

exports.createUser = async function (req, res) {
  var user = await authenticate(req, res);
  if (!user) {
    var addUser = models.user.build({
      userName: req.body.user_name,
      password: req.body.password,
      firstName: req.body.first_name,
      lastName: req.body.last_name,
      email: req.body.email
    });

    addUser.save().then(function (user) {
      req.session.user = user;
      res.redirect('/dashboard');
    });
  }
}

//user login
exports.login = function (req, res) {
  if (req.session && req.session.user) {
    res.redirect('/dashboard')
  } else res.render("session/login.ejs", { error: '' });
}

//session create on login
exports.create = async function (req, res) {
  var userLogin = await models.user.authenticate(req.body.user_name);
  if (!userLogin) {
    res.render('session/login.ejs', { error: 'Invalid user name or password.' });
  } else {
    if (userLogin.validPassword(req.body.password)) {
      // set a cookie with the user's info
      req.session.user = userLogin;
      res.redirect('/dashboard');
    } else {
      res.render('session/login.ejs', { error: 'Invalid user name or password.' });
    }
  }
}

//session reset on logout
exports.delete = function (req, res) {
  req.session.reset();
  res.redirect('/');
}

var authenticate = async function (req, res) {
  //authentication of valid user_name and email
  var user_name = await models.user.authenticate(req.body.user_name);
  var email = await models.user.emailAuthenticate(req.body.email);
  var validEmail = !(IsEmail.validate(req.body.email));
  var passwordCheck = req.body.password != req.body.cpassword;

  if (user_name || email || validEmail || passwordCheck) {
    if (user_name) {
      res.render("session/signup.ejs", { error: 'User name must be unique' });
      return true;
    } else if (passwordCheck) {
      res.render("session/signup.ejs", { error: 'password and confirm password must be same' });
      return true;
    } else {
      res.render("session/signup.ejs", { error: 'email is not valid or not unique' });
      return true;
    }
  } else {
    return false;
  }
}
