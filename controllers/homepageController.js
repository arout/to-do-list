class HOMEPAGECONTROLLER {
  static show(req, res) {
    if (!(req.session && req.session.user)) {
      return res.render('homepage/show.ejs');
    }
    res.redirect('/dashboard');
  }
}

module.exports = HOMEPAGECONTROLLER;