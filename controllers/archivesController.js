const models = require('../models');

class ARCHIVESCONSTROLLER {
  //function to display the archives page
  static async show(req, res){
    if (!(req.session && req.session.user)) {
      return res.redirect('/');
    }
    const cards = await models.card.findAll({
      where: {
        user_id: req.session.user.id,
        archive: true
      }
    });
    const lists = await models.list.findAll({
      include: [
        {
          model: models.card,
          where: { user_id: req.session.user.id }
        }],
      where: { archive: true }
    })
    res.render("archive/show.ejs", { lists: lists, cards: cards, user: req.session.user });
  }
}

module.exports = ARCHIVESCONSTROLLER;