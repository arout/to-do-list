'use strict';
module.exports = (sequelize, DataTypes) => {
  const list = sequelize.define('list', {
    name: DataTypes.STRING,
    archive: DataTypes.BOOLEAN,
    description: DataTypes.STRING

  }, {});
  
  list.findListById = async function (id) {
    return await list.findOne({ where: { id: id } });
  }

  list.associate = function(models) {
    list.belongsTo(models.card,{ foreignKey: 'card_id'})
  };
  return list;
};
