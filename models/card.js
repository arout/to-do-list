'use strict';
module.exports = (sequelize, DataTypes) => {
  const card = sequelize.define('card', {
    name: DataTypes.STRING,
    archive: DataTypes.BOOLEAN,
  }, {});

  card.findCardById = async function (id) {
    return await card.findOne({ where: { id: id } });
  }

  card.associate = function (models) {
    card.belongsTo(models.user, {  foreignKey: 'user_id' });
    card.hasMany(models.list, {  foreignKey: 'card_id' })
  };
  return card;
};
