'use strict';

const bcrypt = require('bcrypt');
const Sequelize = require('sequelize')
const Op = Sequelize.Op;

module.exports = (sequelize, DataTypes) => {
  const user = sequelize.define('user', {
    userName: DataTypes.STRING,
    password: DataTypes.STRING,
    firstName: DataTypes.STRING,
    lastName: DataTypes.STRING,
    email: DataTypes.STRING,
  }, {});

  user.beforeCreate((user, options) => {
    return bcrypt.hash(user.password, 10)
      .then(hash => {
        user.password = hash;
      })
      .catch(err => {
        throw new Error();
      });
  });
  // comparing bcrypt passwords
  user.prototype.validPassword = function (password) {
    return bcrypt.compareSync(password, this.password);
  };

  //finding user from user table based on user name
  user.authenticate = async function (userName) {
    return await user.findOne({ where: { userName: userName } });
  }

  user.authenticateByID = async function (userName, id) {
    return await user.findOne({ where: { userName: userName, id: { [Op.ne]: id } } });
  }

  //finding user from user table based on email
  user.emailAuthenticate = async function (email) {
    return await user.findOne({ where: { email: email } });
  }

  user.emailAuthenticateByID = async function (email, id) {
    return await user.findOne({ where: { email: email, id: { [Op.ne]: id } } });
  }

  user.findUserById = async function (id) {
    return await user.findOne({ where: { id: id } });
  }
  
  user.associate = function (models) {
    user.hasMany(models.card, { as: 'cards', foreignKey: 'user_id' })
  }

  return user;
}

