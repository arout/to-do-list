const models = require('../models');
const IsEmail = require("isemail");


exports.authenticate = async (req, res) => {
  const user_name = await models.user.authenticate(req.body.user_name);
  if (!user_name) {
    return true
  }
}

exports.passwordAuthenticate = async (req, res) => {
  const passwordCheck = await (req.body.password != req.body.cpassword);
  if (!passwordCheck) {
    return true
  }
}

exports.emailAuthenticate = async (req, res) => {
  const email = await models.user.emailAuthenticateByID(req.body.email);
  const validEmail = !(IsEmail.validate(req.body.email));
  if (email || validEmail) {
    return false
  }
  return true
}

exports.authenticateById = async (req, res) => {
  const user_name = await models.user.authenticateByID(req.body.userName, req.session.user.id);
  if (!user_name) {
    return true
  }
}

exports.passwordAuthenticateById = async (req, res) => {
  const passwordCheck = await (req.body.password != req.body.cpassword);
  if (!passwordCheck) {
    return true
  }
}

exports.emailAuthenticateById = async (req, res) => {
  const email = await models.user.emailAuthenticateByID(req.body.email, req.session.user.id);
  const validEmail = !(IsEmail.validate(req.body.email));
  if (email || validEmail) {
    return false
  }
  return true
}

