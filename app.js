const express = require('express');
const bodyParser = require('body-parser');
const session = require('client-sessions');
const methodOverride = require('method-override');
const sessionRouter = require('./routes/session');
const homepageRouter = require('./routes/homepage');
const dashboardRouter = require('./routes/dashboard');
const cardRouter = require('./routes/card');
const listRouter = require('./routes/list');
const userRouter = require('./routes/user');
const archiveRouter = require('./routes/archive');

const app = express();

app.set('view engine', 'ejs');
app.set('views', './views');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: 'true' }));

app.use(methodOverride('_method'));

app.use(express.static(__dirname + '/public'));

app.use(session({
  cookieName: 'session',
  secret: 'thisisnotforyou',
  duration: 30 * 60 * 1000,
  activeDuration: 5 * 60 * 1000,
}));

app.use('/', sessionRouter);
app.use('/', homepageRouter);
app.use('/', dashboardRouter);
app.use('/', cardRouter);
app.use('/', listRouter);
app.use('/', userRouter);
app.use('/', archiveRouter);

app.use(function (req, res, next) {
  res.status(404).send("Sorry can't find that!")
});

app.listen(8080);
